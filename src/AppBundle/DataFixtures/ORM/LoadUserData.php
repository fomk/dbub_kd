<?php
/**
 * Created by PhpStorm.
 * User: fominsk
 * Date: 10.10.2016
 * Time: 10:04
 */

namespace AppBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;

class LoadUserData implements FixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setName('Jane');
        $user->setSurname('Doe');
        $user->setUsername('jane');
        $user->setPassword(password_hash('P@ssw0rd',PASSWORD_DEFAULT));
        $user->setRoles(array('ROLE_ADMIN'));
        $user->setIsActive(true);
        $user->setLanguage('ru');
        $manager->persist($user);
        $manager->flush();
    }
}