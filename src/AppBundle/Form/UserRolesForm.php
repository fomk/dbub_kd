<?php
/**
 * Created by PhpStorm.
 * User: fominsk
 * Date: 10.10.2016
 * Time: 14:49
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserRolesForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $roles = array(
            'ROLE_USER' => 'ROLE_USER',
            'ROLE_ADMIN' => 'ROLE_ADMIN',
        );


        $builder->add('roles', ChoiceType::class,array(
            'choices' => $roles,
            'expanded' => true,
            'multiple' => true,
            'label' => false
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'attr' => array(
                'novalidate' => 'novalidate',
                'autocomplete' => 'off',
                'intention' => 'app_UserRolesForm'
            )
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_UserRolesForm';
    }
}