<?php
/**
 * Created by PhpStorm.
 * User: fominsk
 * Date: 10.10.2016
 * Time: 14:36
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChangePasswordForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('oldPassword',PasswordType::class, array(
            'attr' => array(
                'placeholder' => 'placeholder.enter.old.password'
            ),
            'label' => 'label.current.password',
            'always_empty' => false
        ))->add('clearTextPassword',RepeatedType::class, array(
            'type' => PasswordType::class,
            'invalid_message' => 'message.passwords.not.match',
            'first_options'  => array(
                'label' => 'label.new.password',
                'always_empty' => false,
                'attr' => array(
                    'placeholder' => 'placeholder.enter.new.password'
                ),
            ),
            'second_options' => array(
                'label' => 'label.repeat.new.password',
                'always_empty' => false,
                'attr' => array(
                    'placeholder' => 'placeholder.repeat.new.password'
                ),
            ),
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'attr' => array(
                'novalidate' => 'novalidate',
                'autocomplete' => 'off',
                'intention' => 'app_ChangePasswordForm'
            )
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_ChangePasswordForm';
    }
}