<?php
/**
 * Created by PhpStorm.
 * User: fominsk
 * Date: 01.12.2016
 * Time: 10:56
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AuthorForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class, array(
                'attr' => array(
                    'placeholder' => 'placeholder.enter.name'
                ),
                'label' => 'label.name'
            ))->add('biography',TextareaType::class, array(
                'attr' => array(
                    'placeholder' => 'placeholder.enter.biography',
                    'rows' => 10
                ),
                'label' => 'label.biography'
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'attr' => array(
                'novalidate' => 'novalidate',
                'autocomplete' => 'off',
                'intention' => 'app_AuthorForm'
            )
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_AuthorForm';
    }
}