<?php
/**
 * Created by PhpStorm.
 * User: fominsk
 * Date: 01.12.2016
 * Time: 15:20
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class BookForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class, array(
                'attr' => array(
                    'placeholder' => 'placeholder.enter.name'
                ),
                'label' => 'label.name'
            ))
            ->add('authors', Select2EntityType::class, array(
                'multiple' => true,
                'remote_route' => 'ajax_get_authors_list',
                'class' => 'AppBundle\Entity\Author',
                'primary_key' => 'id',
                'text_property' => 'name',
                'minimum_input_length' => 3,
                'page_limit' => 10,
                'allow_clear' => true,
                'delay' => 250,
                'cache' => true,
                'cache_timeout' => 60000, // if 'cache' is true
                'language' => $options['locale'],
                'placeholder' => 'label.select.author',
                'label' => 'label.authors'
            ))
            ->add('genres', Select2EntityType::class, array(
                'multiple' => true,
                'remote_route' => 'ajax_get_genres_list',
                'class' => 'AppBundle\Entity\Genre',
                'primary_key' => 'id',
                'text_property' => 'name',
                'minimum_input_length' => 3,
                'page_limit' => 10,
                'allow_clear' => true,
                'delay' => 250,
                'cache' => true,
                'cache_timeout' => 60000, // if 'cache' is true
                'language' => $options['locale'],
                'placeholder' => 'label.select.genre',
                'label' => 'label.genres'
            ))
            ->add('description',TextareaType::class, array(
                'attr' => array(
                    'placeholder' => 'placeholder.enter.description',
                    'rows' => 10
                ),
                'label' => 'label.description'
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'attr' => array(
                'novalidate' => 'novalidate',
                'autocomplete' => 'off',
                'intention' => 'app_BookForm',
            ),
            'locale' => 'en'
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_BookForm';
    }
}