<?php
/**
 * Created by PhpStorm.
 * User: fominsk
 * Date: 01.12.2016
 * Time: 11:39
 */

namespace AppBundle\Form;


use libphonenumber\PhoneNumberFormat;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;

class ReaderForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class, array(
                'attr' => array(
                    'placeholder' => 'placeholder.enter.name'
                ),
                'label' => 'label.name'
            ))->add('surname',TextType::class, array(
                'attr' => array(
                    'placeholder' => 'placeholder.enter.surname'
                ),
                'label' => 'label.surname'
            ))->add('phone',PhoneNumberType::class, array(
                'default_region' => 'LV',
                'format' => PhoneNumberFormat::NATIONAL,
                'attr' => array(
                    'placeholder' => 'placeholder.enter.phone'
                ),
                'label' => 'label.phone'
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'attr' => array(
                'novalidate' => 'novalidate',
                'autocomplete' => 'off',
                'intention' => 'app_ReaderForm'
            )
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_ReaderForm';
    }
}