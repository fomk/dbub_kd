<?php
/**
 * Created by PhpStorm.
 * User: fominsk
 * Date: 10.10.2016
 * Time: 14:48
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class, array(
                'attr' => array(
                    'placeholder' => 'placeholder.enter.name'
                ),
                'label' => 'label.name'
            ))
            ->add('surname',TextType::class, array(
                'attr' => array(
                    'placeholder' => 'placeholder.enter.surname'
                ),
                'label' => 'label.surname'
            ))
            ->add('username',TextType::class, array(
                'attr' => array(
                    'placeholder' => 'placeholder.enter.username'
                ),
                'label' => 'label.username'
            ))
            ->add('language', ChoiceType::class,array(
                'choices' => array(
                    'label.english' => 'en',
                    'label.russian' => 'ru',
                ),
                'choices_as_values' => true,
                'label' => 'label.language'
            ))
            ->add('clearTextPassword', PasswordType::class,array(
                'attr' => array(
                    'placeholder' => 'placeholder.enter.password'
                ),
                'label' => 'label.password'
            ))
            ->add('isActive',CheckboxType::class, array(
                'label' => 'label.isActive'
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'attr' => array(
                'novalidate' => 'novalidate',
                'autocomplete' => 'off',
                'intention' => 'app_UserForm'
            )
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_UserForm';
    }
}