<?php
/**
 * Created by PhpStorm.
 * User: fominsk
 * Date: 07.12.2016
 * Time: 14:32
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class BookIssueForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('book', Select2EntityType::class, array(
                'multiple' => false,
                'remote_route' => 'ajax_get_books_list',
                'class' => 'AppBundle\Entity\Book',
                'primary_key' => 'id',
                'text_property' => 'name',
                'minimum_input_length' => 1,
                'page_limit' => 10,
                'allow_clear' => true,
                'delay' => 250,
                'cache' => true,
                'cache_timeout' => 60000, // if 'cache' is true
                'language' => $options['locale'],
                'placeholder' => 'label.select.book',
                'label' => 'label.book',
                'required' => true
            ))
            ->add('reader', Select2EntityType::class, array(
                'multiple' => false,
                'remote_route' => 'ajax_get_readers_list',
                'class' => 'AppBundle\Entity\Reader',
                'primary_key' => 'id',
                'text_property' => 'name',
                'minimum_input_length' => 1,
                'page_limit' => 10,
                'allow_clear' => true,
                'delay' => 250,
                'cache' => true,
                'cache_timeout' => 60000, // if 'cache' is true
                'language' => $options['locale'],
                'placeholder' => 'label.select.reader',
                'label' => 'label.reader',
                'required' => true

            ))
            ->add('due_date',DateType::class, array(
                'widget' => 'single_text',
                'format' => 'dd.MM.yyyy',
                'label' => 'label.due.date',
                'attr' => array(
                    'class' => 'datepicker'
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'attr' => array(
                'novalidate' => 'novalidate',
                'autocomplete' => 'off',
                'intention' => 'app_BookIssueForm',
            ),
            'locale' => 'en'
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_BookIssueForm';
    }
}