<?php
/**
 * Created by PhpStorm.
 * User: fominsk
 * Date: 10.10.2016
 * Time: 10:00
 */

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class MenuBuilder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');
        // Issued books
        $menu->addChild('label.issued.books', array('route' => 'home'))
            ->setAttribute('icon', 'fa fa-folder-open');
        // Readers
        $menu->addChild('label.readers', array('route' => 'readers'))
            ->setAttribute('icon', 'fa fa-group');
        // Books
        $menu->addChild('label.books', array('route' => 'books'))
            ->setAttribute('icon', 'fa fa-book');
        // Authors
        $menu->addChild('label.authors', array('route' => 'authors'))
            ->setAttribute('icon', 'fa fa-star');
        // Genre
        $menu->addChild('label.genres', array('route' => 'genres'))
            ->setAttribute('icon', 'fa fa-tags');

        return $menu;
    }

    public function userMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav pull-right');

        /*
        You probably want to show user specific information such as the username here. That's possible! Use any of the below methods to do this.

        if($this->container->get('security.context')->isGranted(array('ROLE_ADMIN', 'ROLE_USER'))) {} // Check if the visitor has any authenticated roles
        $username = $this->container->get('security.context')->getToken()->getUser()->getUsername(); // Get username of the current logged in user

        */
        $name = $this->container->get('security.token_storage')->getToken()->getUser()->getName() . ' ' .$this->container->get('security.token_storage')->getToken()->getUser()->getSurname();
        //$name = 'asdasd';

        if ($this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            // Librarians
            $menu->addChild('label.users', array('route' => 'users'))
                ->setAttribute('icon', 'fa fa-user-secret');
        }
        // Current user
        $menu->addChild('User', array('label' => $name))
            ->setAttribute('dropdown', true)
            ->setAttribute('icon', 'fa fa-user');
        // Current user profile
        $menu['User']->addChild('label.profile', array('route' => 'profile'))
            ->setAttribute('icon', 'fa fa-gears')
            ->setAttribute('divider_append', true);
        // Current user logoff
        $menu['User']->addChild('label.logout', array('route' => 'logout'))
            ->setAttribute('icon', 'fa fa-sign-out');



        return $menu;
    }
}