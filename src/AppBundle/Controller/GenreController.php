<?php
/**
 * Created by PhpStorm.
 * User: fominsk
 * Date: 10.10.2016
 * Time: 15:20
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Genre;
use AppBundle\Form\GenreForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class GenreController extends Controller
{
    /**
     * @Route("/genres", name="genres", methods={"GET"})
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Genre:index.html.twig');
    }

    /**
     * @Route("/genres/ajax/getGenresTable", name="ajax_get_genres_table", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function ajaxGetGenreTable(Request $request)
    {
        $result = array();
        $result['draw'] = $request->get('draw');
        $offset = $request->get('start');
        $limit = $request->get('length');
        $order = $request->get('order');
        $search_ = $request->get('search');
        $searchValue = $search_['value'];
        $columns = $request->get('columns');

        $orderColumn = $columns[$order[0]['column']]['name'];
        $orderDirection = $order[0]['dir'];

        $em = $this->getDoctrine()->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('g')
            ->from('AppBundle:Genre','g');
        if ($searchValue != '') {
            $search = '%'.$searchValue.'%';
            $qb->where('g.name LIKE :search')
                ->setParameter('search',$search);
        }
        $qb->orderBy($orderColumn,$orderDirection)
            ->setMaxResults($limit)
            ->setFirstResult($offset);

        $genres = $qb->getQuery()->getResult();
        $data = array();
        /**
         * @var Genre $genre
         */
        foreach ($genres as $genre) {

            $options = '<div class="btn-group btn-group-xs" role="group" aria-label="...">';
            $options .= '<a href="'.$this->generateUrl('genres-edit',array( "id" => $genre->getId() )).'" class="btn btn-default"><i class="fa fa-edit"></i></a>';
            $options .= '<a href="'.$this->generateUrl('genres-delete',array( "id" => $genre->getId() )).'" class="btn btn-default cancel"><i class="fa fa-trash-o"></i></a>';
            $options .= '</div>';
            $data[] = array(
                $genre->getName(),
                $genre->getDescription(),
                $options
            );
        }
        $query = $qb->getQuery()->getSQL();
        $split = str_replace('FROM', "<br>FROM",$query);
        $split = str_replace('WHERE', "<br>WHERE",$split);
        $split = str_replace('ORDER', "<br>ORDER",$split);
        $result['query'] = $split;
        $qb = null;
        $qb = $em->createQueryBuilder();
        $result['recordsTotal'] = $qb->select('count(g.id)')->from('AppBundle:Genre', 'g')->getQuery()->getSingleScalarResult();
        $query = $qb->getQuery()->getSQL();
        $query = str_replace(',', ",<br>",$query);
        $query = str_replace('FROM', "<br>FROM",$query);
        $query= str_replace('WHERE', "<br>WHERE",$query);
        $query= str_replace('ORDER', "<br>ORDER",$query);
        $query= str_replace('INNER', "<br>INNER",$query);
        $result['query'] .= '<br><br>'.$query;
        if ($searchValue != '') {
            $qb = null;
            $qb = $em->createQueryBuilder();
            $qb->select('count(g.id)')
                ->from('AppBundle:Genre','g');
            $search = '%'.$searchValue.'%';
            $qb->where('g.name LIKE :search')
                ->setParameter('search',$search);
            $result['recordsFiltered'] = $qb->getQuery()->getSingleScalarResult();
            $query = $qb->getQuery()->getSQL();
            $query = str_replace(',', ",<br>",$query);
            $query = str_replace('FROM', "<br>FROM",$query);
            $query= str_replace('WHERE', "<br>WHERE",$query);
            $query= str_replace('ORDER', "<br>ORDER",$query);
            $query= str_replace('INNER', "<br>INNER",$query);
            $result['query'] .= '<br><br>'.$query;
        } else {
            $result['recordsFiltered'] = $result['recordsTotal'];
        }
        $result['data'] = $data;
        return new JsonResponse($result);
    }

    /**
     * @Route("/genres/add", name="genres-add",methods={"GET", "POST"})
     */
    public function addAction(Request $request)
    {
        $form = $this->createForm( GenreForm::class, new Genre(), array('validation_groups' => array('creation')) );

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $genre = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($genre);
                $em->flush();
                return $this->redirectToRoute('genres');
            }
        }

        return $this->render('AppBundle:Genre:add.html.twig',array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/genres/edit/{id}", name="genres-edit",methods={"GET", "POST"}, requirements={"id" = "\d+"})
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $genre = $em->find('AppBundle:Genre',$id);
        $form = $this->createForm( GenreForm::class, $genre, array('validation_groups' => array('edition')));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $genre = $form->getData();
                $em->merge($genre);
                $em->flush();
                return $this->redirectToRoute('genres');
            }
        }

        return $this->render('AppBundle:Genre:edit.html.twig',array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/genres/delete/{id}", name="genres-delete",methods={"GET", "POST"}, requirements={"id" = "\d+"})
     */
    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $genre = $em->find('AppBundle:Genre',$id);
        $token = $this->get('security.csrf.token_manager');
        if ($request->isMethod('POST')) {
            if ($request->get('token_') === $token->getToken('delete')->getValue()) {
                $em->remove($genre);
                $em->flush();
            }
            return $this->redirectToRoute('genres');
        }

        return $this->render('AppBundle:Genre:delete.html.twig',array(
            'genre' => $genre,
            'token' => $token->refreshToken('delete'),
            'id' => $id
        ));
    }

    /**
     * @Route("/genres/ajax/getGenresList", name="ajax_get_genres_list", methods={"GET"}, condition="request.isXmlHttpRequest()")
     */
    public function ajaxGetGenreList(Request $request)
    {
        $result = array();
        $limit = filter_var($request->get('page_limit'),FILTER_SANITIZE_NUMBER_INT);
        $search = '%'.filter_var(mb_strtolower($request->get('q')),FILTER_SANITIZE_STRING).'%';

        $em = $this->getDoctrine()->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('g')
            ->from('AppBundle:Genre','g')
            ->where('LOWER(g.name) LIKE :search')
            ->setParameter('search',$search);
        $qb->orderBy('g.name','ASC')
            ->setMaxResults($limit);

        $objects = $qb->getQuery()->getResult();
        /** @var Genre $object */
        foreach ($objects as $object)
        {
            $result[] = array(
                'id' => $object->getId(),
                'text' => $object->getName()
            );
        }

        return new JsonResponse($result);
    }

    /**
     * @Route("/genres/ajax/getGenresBookCount", name="ajax_get_genres_book_count", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function ajaxGetGenresBookCount(Request $request)
    {
        $result = array();
        $query = "SELECT 
g0_.name AS label,
count(bg0_.genre_id) AS value
FROM genres AS g0_
LEFT JOIN book_genre AS bg0_ ON (bg0_.genre_id = g0_.id)
WHERE g0_.deleted_at IS NULL
GROUP by g0_.id";
        $em = $this->getDoctrine()->getEntityManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($query);
        $stmt->execute();
        $result['data'] = $stmt->fetchAll();
        $result['query'] = $query;

        return new JsonResponse($result);
    }

    /**
     * @Route("/genres/ajax/getGenresPopularity", name="ajax_get_genres_popularity", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function ajaxGetGenresPopularity(Request $request)
    {
        $result = array();
        $query = "SELECT 
g0_.name AS label,
count(bh0_.book_id) AS value
FROM genres AS g0_
LEFT JOIN book_genre AS bg0_ ON (bg0_.genre_id = g0_.id)
LEFT JOIN book_history AS bh0_ ON (bh0_.book_id = bg0_.book_id)
WHERE g0_.deleted_at IS NULL
GROUP by g0_.id";
        $em = $this->getDoctrine()->getEntityManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($query);
        $stmt->execute();
        $result['data'] = $stmt->fetchAll();
        $result['query'] = $query;

        return new JsonResponse($result);
    }
}