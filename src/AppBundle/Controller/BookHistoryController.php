<?php
/**
 * Created by PhpStorm.
 * User: fominsk
 * Date: 07.12.2016
 * Time: 14:24
 */

namespace AppBundle\Controller;

use AppBundle\Entity\BookHistory;
use AppBundle\Form\BookIssueForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;

class BookHistoryController extends Controller
{

    /**
     * @Route("/", name="home", methods={"GET"})
     */
    public function indexAction()
    {
        return $this->render('AppBundle:BookHistory:index.html.twig');
    }

    /**
     * @Route("/issue/book", name="issue-book",methods={"GET", "POST"})
     */
    public function issueAction(Request $request)
    {
        $form = $this->createForm( BookIssueForm::class, new BookHistory(), array('validation_groups' => array('creation'),'locale' => $request->getLocale()) );

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                /** @var BookHistory $object */
                $object = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $object->setLibrarian($this->getUser());
                $em->persist($object);
                $em->flush();
                return $this->redirectToRoute('home');
            }
        }

        return $this->render('AppBundle:BookHistory:issue.html.twig',array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/issued/ajax/getBooksTable", name="ajax_get_issued_books_table", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function ajaxGetIssuedBooksTable(Request $request)
    {
        $result = array();
        $result['draw'] = $request->get('draw');
        $offset = $request->get('start');
        $limit = $request->get('length');
        $order = $request->get('order');
        $search_ = $request->get('search');
        $search = $search_['value'];
        //$searchValue = mb_strtolower($search_['value']);
        $columns = $request->get('columns');

        $orderColumn = $columns[$order[0]['column']]['name'];
        $orderDirection = strtoupper($order[0]['dir']);

        $em = $this->getDoctrine()->getEntityManager();

        $con = $em->getConnection();
        //$search = '%'.$searchValue.'%';
        $where = 'WHERE bh0_.return_date IS NULL';





        if (!empty($search)) {
            if (ctype_digit($search)) {
                $search = (int)filter_var($search,FILTER_SANITIZE_NUMBER_INT);
                $where .= ' AND (b0_.id = :search OR r0_.id = :search)';
            } else {
                $search = '%'.filter_var(mb_strtolower($search),FILTER_SANITIZE_STRING).'%';
                $where .= ' AND (LOWER(b0_.name) LIKE :search OR LOWER(r0_.name) LIKE :search)';
            }
        }

        $query = "SELECT 
bh0_.id AS id,
bh0_.issue_date AS issue_date,
bh0_.due_date AS due_date,
b0_.id AS book_id,
b0_.name AS book_name,
r0_.id AS reader_id,
r0_.name AS reader_name,
r0_.surname AS reader_surname,
u0_.name AS librarian_name,
u0_.surname AS librarian_surname
FROM book_history AS bh0_
INNER JOIN books AS b0_ ON (b0_.id = bh0_.book_id)
INNER JOIN readers AS r0_ ON (r0_.id = bh0_.reader_id)
INNER JOIN users AS u0_ ON (u0_.id = bh0_.librarian_id)
".$where."
ORDER BY ".$orderColumn.' '.$orderDirection . ' LIMIT ' . $limit . ' OFFSET '.$offset;

        $stmt = $con->prepare($query);

        if (!empty($search)) {
            $stmt->bindValue('search',$search);
        }
        $stmt->execute();
        $objects = $stmt->fetchAll();

        $result['query'] = $query;


        $data = array();
        $translator = $this->container->get('translator');
        $current_date = new \DateTime('now');
        /**
         * @var BookHistory $object
         */
        foreach ($objects as $object) {
            $options = '<button type="button" class="btn btn-info btn-xs return-button" style="width: 100%">'.$translator->trans('label.return').'</button>';
            $due_date = new \DateTime($object['due_date']);
            $issued_date = new \DateTime($object['issue_date']);
            $limit = (int)$issued_date->diff($due_date)->format('%a') + 1;
            $now = $issued_date->diff($current_date)->format('%a');
            $status = $now * 100 / $limit;
            $class = ' progress-bar progress-bar-danger';
            if ($status < 50) {
                $class = ' progress-bar-success';
            } elseif ($status < 70) {
                $class = '';
            } elseif($status < 90) {
                $class = ' progress-bar progress-bar-warning';
            }
            $status_progress = '<div class="progress" style="margin-bottom: 0 !important;"><div class="progress-bar'.$class.'" role="progressbar" aria-valuenow="'.$status.'" aria-valuemin="0" aria-valuemax="100" style="width: '.$status.'%;"></div></div>';

            $data[] = array(
                'DT_RowId' => $object['id'],
                '['.$object['book_id'] .'] '. $object['book_name'],
                '['.$object['reader_id'] .'] '. $object['reader_name'] . ' ' . $object['reader_surname'],
                $object['librarian_name'] . ' ' . $object['librarian_surname'],
                '<i class="fa fa-calendar"></i> ' . $issued_date->format('d.m.Y') . '&nbsp;&nbsp;<i class="fa fa-clock-o"></i> ' . $issued_date->format('H:i') ,
                '<i class="fa fa-calendar"></i> ' . $due_date->format('d.m.Y'),
                $status_progress,
                $options
            );
        }
        $qb = null;
        $qb = $em->createQueryBuilder();

        $result['recordsTotal'] = $qb->select('count(bh.id)')->from('AppBundle:BookHistory', 'bh')->where('bh.return_date IS NULL')->getQuery()->getSingleScalarResult();

        $query = $qb->getQuery()->getSQL();
        $query = str_replace(',', ",<br>",$query);
        $query = str_replace('FROM', "<br>FROM",$query);
        $query= str_replace('WHERE', "<br>WHERE",$query);
        $query= str_replace('ORDER', "<br>ORDER",$query);
        $query= str_replace('INNER', "<br>INNER",$query);
        $result['query'] .= '<br><br>'.$query;

        if (!empty($search)) {

            $query = "SELECT 
count(*) AS filtered_row_count
FROM book_history AS bh0_
INNER JOIN books AS b0_ ON (b0_.id = bh0_.book_id)
INNER JOIN readers AS r0_ ON (r0_.id = bh0_.reader_id)
".$where.";";


            $stmt = $con->prepare($query);
            if (!empty($search)) {
                $stmt->bindValue('search',$search);
            }
            $stmt->execute();
            $count = $stmt->fetchColumn(0);
            $result['recordsFiltered'] = $count;
            $result['query'] .= '<br><br>'.$query;
        } else {
            $result['recordsFiltered'] = $result['recordsTotal'];
        }
        $result['data'] = $data;



        return new JsonResponse($result);
    }

    /**
     * @Route("/issued/ajax/returnBook", name="ajax_return_book", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function ajaxReturnBook(Request $request)
    {

        $id = filter_var($request->get('id'),FILTER_SANITIZE_NUMBER_INT);

        $em = $this->getDoctrine()->getManager();

        /** @var BookHistory $object */
        $object = $em->find('AppBundle:BookHistory', $id);
        $object->setReturnDate(new \DateTime('now'));

        $em->merge($object);
        $em->flush();

        return new JsonResponse('Ok');
    }

    /**
     * @Route("/issued/ajax/getReaderBooksTable", name="ajax_get_reader_current_books", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function ajaxGetReaderBooksTable(Request $request)
    {
        $result = array();
        $result['draw'] = $request->get('draw');
        $search = filter_var($request->get('id'),FILTER_SANITIZE_NUMBER_INT);

        $em = $this->getDoctrine()->getEntityManager();

        $con = $em->getConnection();

        $where = '';

        $query = "SELECT 
bh0_.id AS id,
bh0_.issue_date AS issue_date,
bh0_.due_date AS due_date,
b0_.id AS book_id,
b0_.name AS book_name,
r0_.id AS reader_id
FROM book_history AS bh0_
INNER JOIN books AS b0_ ON (b0_.id = bh0_.book_id)
INNER JOIN readers AS r0_ ON (r0_.id = bh0_.reader_id)
WHERE bh0_.return_date IS NULL AND r0_.id = :search
ORDER BY b0_.name ASC";

        $stmt = $con->prepare($query);
        $stmt->bindValue('search',$search);

        $stmt->execute();
        $objects = $stmt->fetchAll();

        $result['query'] = $query;


        $data = array();

        $translator = $this->container->get('translator');
        $current_date = new \DateTime('now');
        /**
         * @var BookHistory $object
         */
        foreach ($objects as $object) {
            $options = '<button type="button" class="btn btn-info btn-xs return-button" style="width: 100%">'.$translator->trans('label.return').'</button>';
            $due_date = new \DateTime($object['due_date']);
            $issued_date = new \DateTime($object['issue_date']);
            $limit = (int)$issued_date->diff($due_date)->format('%a') + 1;
            $now = $issued_date->diff($current_date)->format('%a');
            $status = $now * 100 / $limit;
            $class = ' progress-bar progress-bar-danger';
            if ($status < 50) {
                $class = ' progress-bar-success';
            } elseif ($status < 70) {
                $class = '';
            } elseif($status < 90) {
                $class = ' progress-bar progress-bar-warning';
            }
            $status_progress = '<div class="progress" style="margin-bottom: 0 !important;"><div class="progress-bar'.$class.'" role="progressbar" aria-valuenow="'.$status.'" aria-valuemin="0" aria-valuemax="100" style="width: '.$status.'%;"></div></div>';

            $data[] = array(
                'DT_RowId' => $object['id'],
                '['.$object['book_id'] .'] '. $object['book_name'],
                $status_progress,
                $options
            );
        }

        $result['data'] = $data;

        return new JsonResponse($result);
    }

    /**
     * @Route("/issued/ajax/getReaderBookHistoryTable", name="ajax_get_reader_issued_books_history", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function ajaxGetReaderBookHistoryTable(Request $request)
    {
        $result = array();
        $result['draw'] = $request->get('draw');
        $offset = $request->get('start');
        $search = filter_var($request->get('id'),FILTER_SANITIZE_NUMBER_INT);
        $limit = $request->get('length');
        $em = $this->getDoctrine()->getEntityManager();
        $con = $em->getConnection();


        $query = "SELECT 
bh0_.id AS id,
bh0_.issue_date AS issue_date,
bh0_.due_date AS due_date,
bh0_.return_date AS return_date,
b0_.id AS book_id,
b0_.name AS book_name,
r0_.id AS reader_id
FROM book_history AS bh0_
INNER JOIN books AS b0_ ON (b0_.id = bh0_.book_id)
INNER JOIN readers AS r0_ ON (r0_.id = bh0_.reader_id)
WHERE bh0_.return_date IS NOT NULL AND r0_.id = :search
ORDER BY bh0_.due_date DESC" . ' LIMIT ' . $limit . ' OFFSET '.$offset;

        $stmt = $con->prepare($query);
        $stmt->bindValue('search',$search);
        $stmt->execute();
        $objects = $stmt->fetchAll();
        $result['query'] = $query;

        $data = array();
        $translator = $this->container->get('translator');

        /**
         * @var BookHistory $object
         */
        foreach ($objects as $object) {
            $options = '<button type="button" class="btn btn-info btn-xs return-button" style="width: 100%">'.$translator->trans('label.return').'</button>';
            $due_date = new \DateTime($object['due_date']);
            $issued_date = new \DateTime($object['issue_date']);
            $return_date = new \DateTime($object['return_date']);
            $limit = (int)$issued_date->diff($due_date)->format('%a') + 1;
            $returned = $issued_date->diff($return_date)->format('%a');
            if ($returned > $limit) {
                $status = '<span class="label label-danger">' . $translator->trans('label.overdue') . '</span>';
            } else {
                $status = '<span class="label label-success">' . $translator->trans('label.in.time') . '</span>';
            }




            $data[] = array(
                'DT_RowId' => $object['id'],
                '['.$object['book_id'] .'] '. $object['book_name'],

                '<i class="fa fa-calendar"></i> ' . $issued_date->format('d.m.Y'),
                $status,
            );
        }
        $qb = null;
        $qb = $em->createQueryBuilder();
        $result['recordsTotal'] = $qb->select('count(bh.id)')
            ->from('AppBundle:BookHistory', 'bh')
            ->innerJoin('bh.reader', 'r')
            ->where('bh.return_date IS NOT NULL')->andWhere('r.id = :search')->setParameter('search',$search)->getQuery()->getSingleScalarResult();
        $query = $qb->getQuery()->getSQL();
        $query = str_replace(',', ",<br>",$query);
        $query = str_replace('FROM', "<br>FROM",$query);
        $query= str_replace('WHERE', "<br>WHERE",$query);
        $query= str_replace('ORDER', "<br>ORDER",$query);
        $query= str_replace('INNER', "<br>INNER",$query);
        $result['query'] .= '<br><br>'. $query;
        $result['recordsFiltered'] = $result['recordsTotal'];
        $result['data'] = $data;

        return new JsonResponse($result);
    }

    /**
     * @Route("/issued/ajax/getBookReaderHistoryTable", name="ajax_get_book_readers_history_table", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function ajaxGetBookReadersHistoryTable(Request $request)
    {
        $result = array();
        $result['draw'] = $request->get('draw');
        $offset = $request->get('start');
        $search = filter_var($request->get('id'),FILTER_SANITIZE_NUMBER_INT);
        $limit = $request->get('length');
        $em = $this->getDoctrine()->getEntityManager();
        $con = $em->getConnection();


        $query = "SELECT 
bh0_.id AS id,
bh0_.issue_date AS issue_date,
bh0_.due_date AS due_date,
bh0_.return_date AS return_date,
b0_.id AS book_id,
r0_.id AS reader_id,
r0_.name AS reader_name,
r0_.surname AS reader_surname
FROM book_history AS bh0_
INNER JOIN books AS b0_ ON (b0_.id = bh0_.book_id)
INNER JOIN readers AS r0_ ON (r0_.id = bh0_.reader_id)
WHERE bh0_.return_date IS NOT NULL AND b0_.id = :search
ORDER BY bh0_.due_date DESC" . ' LIMIT ' . $limit . ' OFFSET '.$offset;

        $stmt = $con->prepare($query);
        $stmt->bindValue('search',$search);
        $stmt->execute();
        $objects = $stmt->fetchAll();
        $result['query'] = $query;

        $data = array();
        $translator = $this->container->get('translator');

        /**
         * @var BookHistory $object
         */
        foreach ($objects as $object) {
            $options = '<button type="button" class="btn btn-info btn-xs return-button" style="width: 100%">'.$translator->trans('label.return').'</button>';
            $due_date = new \DateTime($object['due_date']);
            $issued_date = new \DateTime($object['issue_date']);
            $return_date = new \DateTime($object['return_date']);
            $limit = (int)$issued_date->diff($due_date)->format('%a') + 1;
            $returned = $issued_date->diff($return_date)->format('%a');
            if ($returned > $limit) {
                $status = '<span class="label label-danger">' . $translator->trans('label.overdue') . '</span>';
            } else {
                $status = '<span class="label label-success">' . $translator->trans('label.in.time') . '</span>';
            }




            $data[] = array(
                'DT_RowId' => $object['id'],
                '['.$object['reader_id'] .'] '. $object['reader_name'] . ' ' . $object['reader_surname'],

                '<i class="fa fa-calendar"></i> ' . $issued_date->format('d.m.Y'),
                $status,
            );
        }
        $qb = null;
        $qb = $em->createQueryBuilder();
        $result['recordsTotal'] = $qb->select('count(bh.id)')
            ->from('AppBundle:BookHistory', 'bh')
            ->innerJoin('bh.book', 'b')
            ->where('bh.return_date IS NOT NULL')->andWhere('b.id = :search')->setParameter('search',$search)->getQuery()->getSingleScalarResult();
        $query = $qb->getQuery()->getSQL();
        $query = str_replace(',', ",<br>",$query);
        $query = str_replace('FROM', "<br>FROM",$query);
        $query= str_replace('WHERE', "<br>WHERE",$query);
        $query= str_replace('ORDER', "<br>ORDER",$query);
        $query= str_replace('INNER', "<br>INNER",$query);
        $result['query'] .= '<br><br>'. $query;
        $result['recordsFiltered'] = $result['recordsTotal'];
        $result['data'] = $data;

        return new JsonResponse($result);
    }

}