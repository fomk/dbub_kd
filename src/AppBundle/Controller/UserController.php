<?php
/**
 * Created by PhpStorm.
 * User: fominsk
 * Date: 10.10.2016
 * Time: 14:47
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserForm;
use AppBundle\Form\UserRolesForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{

    /**
     * @Route("/users", name="users", methods={"GET"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function indexAction()
    {
        return $this->render('AppBundle:User:index.html.twig');
    }

    /**
     * @Route("/users/ajax/getUsersTable", name="ajax_get_users_table", methods={"POST"}, condition="request.isXmlHttpRequest()")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function ajaxGetUsersTable(Request $request)
    {
        $result = array();
        $result['draw'] = $request->get('draw');
        $offset = $request->get('start');
        $limit = $request->get('length');
        $order = $request->get('order');
        $search_ = $request->get('search');
        $searchValue = $search_['value'];
        $columns = $request->get('columns');

        $orderColumn = $columns[$order[0]['column']]['name'];
        $orderDirection = $order[0]['dir'];

        $em = $this->getDoctrine()->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('u')
            ->from('AppBundle:User','u');
        if ($searchValue != '') {
            $search = '%'.$searchValue.'%';
            $qb->where('u.name LIKE :search')
                ->setParameter('search',$search);
        }
        $qb->orderBy($orderColumn,$orderDirection)
            ->setMaxResults($limit)
            ->setFirstResult($offset);

        $users = $qb->getQuery()->getResult();
        $data = array();
        $translator = $this->get('translator');
        $LoggedUser = $this->getUser();
        /**
         * @var User $user
         */
        foreach ($users as $user) {
            if ($user->isEnabled()) {
                $state = '<span class="text-success">'.$translator->trans('label.active').'</span>';
            } else {
                $state = '<span class="text-danger">'.$translator->trans('label.inactive').'</span>';
            }
            $options = '<div class="btn-group btn-group-xs" role="group" aria-label="...">';

            if ($LoggedUser->getId() !== $user->getId()) {
                $options .= '<a href="'.$this->generateUrl('users-edit',array( "id" => $user->getId() )).'" class="btn btn-default"><i class="fa fa-edit"></i></a>';
                $options .= '<a href="'.$this->generateUrl('roles-edit',array( "id" => $user->getId() )).'" class="btn btn-default"><i class="fa fa-group"></i></a>';
                $options .= '<a href="'.$this->generateUrl('users-delete',array( "id" => $user->getId() )).'" class="btn btn-default cancel"><i class="fa fa-trash-o"></i></a>';
            } else {
                $options .= '<a href="'.$this->generateUrl('profile').'" class="btn btn-default"><i class="fa fa-cogs"></i></a>';
            }

            $options .= '</div>';
            $roles = array();
            foreach ($user->getRoles() as $role) {
                $roles[] = $translator->trans($role);
            }
            $data[] = array(
                $user->getName() . ' ' . $user->getSurname(),
                $user->getUsername(),
                $roles,
                $state,
                $options
            );
        }
        $query = $qb->getQuery()->getSQL();
        $query = str_replace(',', ",<br>",$query);
        $query = str_replace('FROM', "<br>FROM",$query);
        $query= str_replace('WHERE', "<br>WHERE",$query);
        $query= str_replace('ORDER', "<br>ORDER",$query);
        $query= str_replace('INNER', "<br>INNER",$query);
        $result['query'] = $query;
        $qb = null;
        $qb = $em->createQueryBuilder();
        $result['recordsTotal'] = $qb->select('count(u.id)')->from('AppBundle:User', 'u')->getQuery()->getSingleScalarResult();
        $query = $qb->getQuery()->getSQL();
        $query = str_replace(',', ",<br>",$query);
        $query = str_replace('FROM', "<br>FROM",$query);
        $query= str_replace('WHERE', "<br>WHERE",$query);
        $query= str_replace('ORDER', "<br>ORDER",$query);
        $query= str_replace('INNER', "<br>INNER",$query);
        $result['query'] .= '<br><br>'.$query;
        if ($searchValue != '') {
            $qb = null;
            $qb = $em->createQueryBuilder();
            $qb->select('count(u.id)')
                ->from('AppBundle:User','u');
            $search = '%'.$searchValue.'%';
            $qb->where('u.name LIKE :search')
                ->setParameter('search',$search);
            $result['recordsFiltered'] = $qb->getQuery()->getSingleScalarResult();
            $query = $qb->getQuery()->getSQL();
            $query = str_replace(',', ",<br>",$query);
            $query = str_replace('FROM', "<br>FROM",$query);
            $query= str_replace('WHERE', "<br>WHERE",$query);
            $query= str_replace('ORDER', "<br>ORDER",$query);
            $query= str_replace('INNER', "<br>INNER",$query);
            $result['query'] .= '<br><br>'.$query;
        } else {
            $result['recordsFiltered'] = $result['recordsTotal'];
        }

        $result['data'] = $data;
        //$result['recordsFiltered'] = $result['recordsTotal'] = 0;
        return new JsonResponse($result);
    }

    /**
     * @Route("/users/add", name="users-add",methods={"GET", "POST"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function addAction(Request $request)
    {
        $form = $this->createForm( UserForm::class, new User(), array('validation_groups' => array('creation')) );

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $user = $form->getData();
                $encoder = $this->container->get('security.password_encoder');
                $user->setPassword($encoder->encodePassword($user,$user->getClearTextPassword()));
                //$user->setRoles(array($user->getRole()));
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                return $this->redirectToRoute('users');
            }
        }

        return $this->render('AppBundle:User:add.html.twig',array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/users/edit/{id}", name="users-edit",methods={"GET", "POST"}, requirements={"id" = "\d+"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->find('AppBundle:User',$id);
        $form = $this->createForm( UserForm::class, $user, array('validation_groups' => array('edition')));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $user = $form->getData();

                if (!empty($user->getClearTextPassword())) {
                    $encoder = $this->container->get('security.password_encoder');
                    $user->setPassword($encoder->encodePassword($user,$user->getClearTextPassword()));
                }
                $em->merge($user);
                $em->flush();
                return $this->redirectToRoute('users');
            }
        }

        return $this->render('AppBundle:User:edit.html.twig',array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/users/delete/{id}", name="users-delete",methods={"GET", "POST"}, requirements={"id" = "\d+"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction($id, Request $request)
    {
        $LoggedUser = $this->getUser();
        if ($id === $LoggedUser->getId()) {
            return $this->redirectToRoute('users');
        }
        $em = $this->getDoctrine()->getManager();
        $user = $em->find('AppBundle:User',$id);
        $token = $this->get('security.csrf.token_manager');
        if ($request->isMethod('POST')) {
            if ($request->get('token_') === $token->getToken('delete')->getValue()) {
                $em->remove($user);
                $em->flush();
            }
            return $this->redirectToRoute('users');
        }

        return $this->render('AppBundle:User:delete.html.twig',array(
            'user' => $user,
            'token' => $token->refreshToken('delete'),
            'id' => $id
        ));
    }

    /**
     * @Route("/users/roles/{id}", name="roles-edit",methods={"GET", "POST"}, requirements={"id" = "\d+"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editRolesAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->find('AppBundle:User',$id);
        $form = $this->createForm( UserRolesForm::class, $user, array('validation_groups' => array('edition')));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $user = $form->getData();
                $em->merge($user);
                $em->flush();
                return $this->redirectToRoute('users');
            }
        }

        return $this->render('AppBundle:User:editRoles.html.twig',array(
            'form' => $form->createView(),
            'user' => $user
        ));
    }
}