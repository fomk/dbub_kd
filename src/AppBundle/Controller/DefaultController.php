<?php

namespace AppBundle\Controller;

use AppBundle\Form\ChangePasswordForm;
use AppBundle\Form\ProfileForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        array(
            'last_username' => $lastUsername,
            'error'         => $error,
        );
        return $this->render('AppBundle:Default:login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }

    /**
     * @Route("/login_check", name="login_check")
     */
    public function loginCheckAction()
    {
        // this controller will not be executed,
        // as the route is handled by the Security system
    }

    /**
     * @Route("/profile", name="profile", methods={"GET","POST"})
     */
    public function profileAction(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm( ProfileForm::class, $user, array('validation_groups' => array('profile')) );
        $passwordForm = $this->createForm( ChangePasswordForm::class, $user, array('validation_groups' => array('password_change')) );
        $form->handleRequest($request);
        $passwordForm ->handleRequest($request);
        $translator = $this->get('translator');
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');
        if ($form->isSubmitted() && $form->isValid()) {
            $em->merge($user);
            $em->flush();
            $session->set('_locale', $user->getLanguage());
            $session->getFlashBag()->add('profile_notice', $translator->trans('message.profile.updated',array(),'messages', $user->getLanguage()));
            return $this->redirectToRoute('profile');
        }
        if ($passwordForm->isSubmitted() && $passwordForm->isValid()) {
            $encoder = $this->container->get('security.password_encoder');
            $user->setPassword($encoder->encodePassword($user,$user->getClearTextPassword()));
            $em->merge($user);
            $em->flush();
            $session->getFlashBag()->add('password_notice', $translator->trans('message.password.changed',array(),'messages', $user->getLanguage()));
            return $this->redirectToRoute('profile');
        }
        //var_dump($session->get('_locale'));
        //die;
        return $this->render('AppBundle:Default:profile.html.twig', array(
            'form' => $form->createView(),
            'passwordForm' => $passwordForm->createView()
        ));
    }
}
