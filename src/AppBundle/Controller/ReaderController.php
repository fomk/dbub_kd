<?php
/**
 * Created by PhpStorm.
 * User: fominsk
 * Date: 10.10.2016
 * Time: 15:22
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Reader;
use AppBundle\Form\ReaderForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ReaderController extends Controller
{
    /**
     * @Route("/readers", name="readers", methods={"GET"})
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Reader:index.html.twig');
    }

    /**
     * @Route("/readers/ajax/getReadersTable", name="ajax_get_readers_table", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function ajaxGetReadersTable(Request $request)
    {
        $result = array();
        $result['draw'] = $request->get('draw');
        $offset = $request->get('start');
        $limit = $request->get('length');
        $order = $request->get('order');
        $search_ = $request->get('search');
        $searchValue = $search_['value'];
        $columns = $request->get('columns');

        $orderColumn = $columns[$order[0]['column']]['name'];
        $orderDirection = $order[0]['dir'];

        $em = $this->getDoctrine()->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('r')
            ->from('AppBundle:Reader','r');
        if ($searchValue != '') {
            $search = '%'.$searchValue.'%';
            $qb->where('r.name LIKE :search')
                ->setParameter('search',$search);
        }
        $qb->orderBy($orderColumn,$orderDirection)
            ->setMaxResults($limit)
            ->setFirstResult($offset);

        $objects = $qb->getQuery()->getResult();
        $data = array();
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        /**
         * @var Reader $object
         */
        foreach ($objects as $object) {

            $options = '<div class="btn-group btn-group-xs" role="group" aria-label="...">';
            $options .= '<a href="'.$this->generateUrl('readers-info',array( "id" => $object->getId() )).'" class="btn btn-default"><i class="fa fa-info-circle"></i></a>';
            $options .= '<a href="'.$this->generateUrl('readers-edit',array( "id" => $object->getId() )).'" class="btn btn-default"><i class="fa fa-edit"></i></a>';
            $options .= '<a href="'.$this->generateUrl('readers-delete',array( "id" => $object->getId() )).'" class="btn btn-default cancel"><i class="fa fa-trash-o"></i></a>';
            $options .= '</div>';
            $data[] = array(
                $object->getId(),
                $object->getName(),
                $object->getSurname(),
                $phoneUtil->format($object->getPhone(),\libphonenumber\PhoneNumberFormat::INTERNATIONAL),
                $options
            );
        }
        $query = $qb->getQuery()->getSQL();
        $split = str_replace('FROM', "<br>FROM",$query);
        $split = str_replace('WHERE', "<br>WHERE",$split);
        $split = str_replace('ORDER', "<br>ORDER",$split);
        $result['query'] = $split;
        $qb = null;
        $qb = $em->createQueryBuilder();
        $result['recordsTotal'] = $qb->select('count(r.id)')->from('AppBundle:Reader', 'r')->getQuery()->getSingleScalarResult();
        $query = $qb->getQuery()->getSQL();
        $query = str_replace(',', ",<br>",$query);
        $query = str_replace('FROM', "<br>FROM",$query);
        $query= str_replace('WHERE', "<br>WHERE",$query);
        $query= str_replace('ORDER', "<br>ORDER",$query);
        $query= str_replace('INNER', "<br>INNER",$query);
        $result['query'] .= '<br><br>'.$query;
        if ($searchValue != '') {
            $qb = null;
            $qb = $em->createQueryBuilder();
            $qb->select('count(r.id)')
                ->from('AppBundle:Reader','r');
            $search = '%'.$searchValue.'%';
            $qb->where('r.name LIKE :search')
                ->setParameter('search',$search);
            $result['recordsFiltered'] = $qb->getQuery()->getSingleScalarResult();
            $query = $qb->getQuery()->getSQL();
            $query = str_replace(',', ",<br>",$query);
            $query = str_replace('FROM', "<br>FROM",$query);
            $query= str_replace('WHERE', "<br>WHERE",$query);
            $query= str_replace('ORDER', "<br>ORDER",$query);
            $query= str_replace('INNER', "<br>INNER",$query);
            $result['query'] .= '<br><br>'.$query;
        } else {
            $result['recordsFiltered'] = $result['recordsTotal'];
        }
        $result['data'] = $data;
        return new JsonResponse($result);
    }

    /**
     * @Route("/readers/add", name="readers-add",methods={"GET", "POST"})
     */
    public function addAction(Request $request)
    {
        $form = $this->createForm( ReaderForm::class, new Reader(), array('validation_groups' => array('creation')) );

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $object = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($object);
                $em->flush();
                return $this->redirectToRoute('readers');
            }
        }

        return $this->render('AppBundle:Reader:add.html.twig',array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/readers/edit/{id}", name="readers-edit",methods={"GET", "POST"}, requirements={"id" = "\d+"})
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $object = $em->find('AppBundle:Reader',$id);
        $form = $this->createForm( ReaderForm::class, $object, array('validation_groups' => array('edition')));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $object = $form->getData();
                $em->merge($object);
                $em->flush();
                return $this->redirectToRoute('readers');
            }
        }

        return $this->render('AppBundle:Reader:edit.html.twig',array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/readers/delete/{id}", name="readers-delete",methods={"GET", "POST"}, requirements={"id" = "\d+"})
     */
    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $object = $em->find('AppBundle:Reader',$id);
        $token = $this->get('security.csrf.token_manager');
        if ($request->isMethod('POST')) {
            if ($request->get('token_') === $token->getToken('delete')->getValue()) {
                $em->remove($object);
                $em->flush();
            }
            return $this->redirectToRoute('readers');
        }

        return $this->render('AppBundle:Reader:delete.html.twig',array(
            'object' => $object,
            'token' => $token->refreshToken('delete'),
            'id' => $id
        ));
    }

    /**
     * @Route("/readers/info/{id}", name="readers-info",methods={"GET"}, requirements={"id" = "\d+"})
     */
    public function infoAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        //$object = $em->find('AppBundle:Reader',$id);
        $qb = $em->createQueryBuilder();

        $qb->select('r')
            ->from('AppBundle:Reader','r')
            ->where('r.id = ?1')
            ->setParameter(1,$id);
        $object = $qb->getQuery()->getSingleResult();

        $query = $qb->getQuery()->getSQL();
        $query = str_replace(',', ",<br>",$query);
        $query = str_replace('FROM', "<br>FROM",$query);
        $query= str_replace('WHERE', "<br>WHERE",$query);
        $query= str_replace('ORDER', "<br>ORDER",$query);




        //$query1 = $em->createQuery()->getSQL();

        return $this->render('AppBundle:Reader:info.html.twig',array(
            'object' => $object,
            'query' => $query
        ));
    }

    /**
     * @Route("/readers/ajax/getReadersList", name="ajax_get_readers_list", methods={"GET"}, condition="request.isXmlHttpRequest()")
     */
    public function ajaxGetReadersList(Request $request)
    {
        $result = array();
        $limit = filter_var($request->get('page_limit'),FILTER_SANITIZE_NUMBER_INT);

        $em = $this->getDoctrine()->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('r')
            ->from('AppBundle:Reader','r');
        if (ctype_digit($request->get('q'))) {
            $search = (int)filter_var(mb_strtolower($request->get('q')),FILTER_SANITIZE_NUMBER_INT);
            $qb->where('r.id = :search');
        } else {
            $search = '%'.filter_var(mb_strtolower($request->get('q')),FILTER_SANITIZE_STRING).'%';
            $qb->where('LOWER(r.name) LIKE :search')->orWhere('LOWER(r.surname) LIKE :search');
        }
        $qb->setParameter('search',$search)
            ->orderBy('r.name','ASC')
            ->setMaxResults($limit);

        $objects = $qb->getQuery()->getResult();
        /** @var Reader $object */
        foreach ($objects as $object)
        {
            $result[] = array(
                'id' => $object->getId(),
                'text' => '[' . $object->getId() . '] '. $object->getName() . ' ' . $object->getSurname()
            );
        }

        return new JsonResponse($result);
    }

    /**
     * @Route("/readers/ajax/getReaderStats", name="ajax_get_reader_stats", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function ajaxGetReaderStats(Request $request)
    {
        $result = array();
        $id = filter_var($request->get('id'),FILTER_SANITIZE_NUMBER_INT);

        $query = "SELECT 
to_char(date_trunc('month',bh0_.issue_date),'YYYY-MM') AS month,
count(*) AS count
FROM book_history AS bh0_
WHERE bh0_.reader_id = :id
GROUP by month
LIMIT 6";
        $em = $this->getDoctrine()->getEntityManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($query);
        $stmt->bindValue('id',$id);
        $stmt->execute();
        $result['data'] = $stmt->fetchAll();
        $result['query'] = $query;

        return new JsonResponse($result);
    }

}