<?php
/**
 * Created by PhpStorm.
 * User: fominsk
 * Date: 10.10.2016
 * Time: 15:22
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Author;
use AppBundle\Entity\Book;
use AppBundle\Form\AuthorForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AuthorController extends Controller
{
    /**
     * @Route("/authors", name="authors", methods={"GET"})
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Author:index.html.twig');
    }

    /**
     * @Route("/authors/ajax/getAuthorsTable", name="ajax_get_authors_table", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function ajaxGetAuthorsTable(Request $request)
    {
        $result = array();
        $result['draw'] = $request->get('draw');
        $offset = $request->get('start');
        $limit = $request->get('length');
        $order = $request->get('order');
        $search_ = $request->get('search');
        $searchValue = $search_['value'];
        $columns = $request->get('columns');

        $orderColumn = $columns[$order[0]['column']]['name'];
        $orderDirection = $order[0]['dir'];

        $em = $this->getDoctrine()->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('a')
            ->from('AppBundle:Author','a');
        if ($searchValue != '') {
            $search = '%'.$searchValue.'%';
            $qb->where('a.name LIKE :search')
                ->setParameter('search',$search);
        }
        $qb->orderBy($orderColumn,$orderDirection)
            ->setMaxResults($limit)
            ->setFirstResult($offset);

        $objects = $qb->getQuery()->getResult();

        $data = array();
        /**
         * @var Author $object
         */
        foreach ($objects as $object) {

            $options = '<div class="btn-group btn-group-xs" role="group" aria-label="...">';
            $options .= '<a href="'.$this->generateUrl('authors-info',array( "id" => $object->getId() )).'" class="btn btn-default"><i class="fa fa-info-circle"></i></a>';
            $options .= '<a href="'.$this->generateUrl('authors-edit',array( "id" => $object->getId() )).'" class="btn btn-default"><i class="fa fa-edit"></i></a>';
            $options .= '<a href="'.$this->generateUrl('authors-delete',array( "id" => $object->getId() )).'" class="btn btn-default cancel"><i class="fa fa-trash-o"></i></a>';
            $options .= '</div>';
            $data[] = array(
                $object->getName(),
                $object->getBiography(),
                $options
            );
        }


        $query = $qb->getQuery()->getSQL();
        $split = str_replace('FROM', "<br>FROM",$query);
        $split = str_replace('WHERE', "<br>WHERE",$split);
        $split = str_replace('ORDER', "<br>ORDER",$split);
        $result['query'] = $split;

        $qb = null;
        $qb = $em->createQueryBuilder();
        $result['recordsTotal'] = $qb->select('count(a.id)')->from('AppBundle:Author', 'a')->getQuery()->getSingleScalarResult();
        $query = $qb->getQuery()->getSQL();
        $query = str_replace(',', ",<br>",$query);
        $query = str_replace('FROM', "<br>FROM",$query);
        $query= str_replace('WHERE', "<br>WHERE",$query);
        $query= str_replace('ORDER', "<br>ORDER",$query);
        $query= str_replace('INNER', "<br>INNER",$query);
        $result['query'] .= '<br><br>'.$query;
        if ($searchValue != '') {
            $qb = null;
            $qb = $em->createQueryBuilder();
            $qb->select('count(a.id)')
                ->from('AppBundle:Author','a');
            $search = '%'.$searchValue.'%';
            $qb->where('a.name LIKE :search')
                ->setParameter('search',$search);
            $result['recordsFiltered'] = $qb->getQuery()->getSingleScalarResult();
            $query = $qb->getQuery()->getSQL();
            $query = str_replace(',', ",<br>",$query);
            $query = str_replace('FROM', "<br>FROM",$query);
            $query= str_replace('WHERE', "<br>WHERE",$query);
            $query= str_replace('ORDER', "<br>ORDER",$query);
            $query= str_replace('INNER', "<br>INNER",$query);
            $result['query'] .= '<br><br>'.$query;
        } else {
            $result['recordsFiltered'] = $result['recordsTotal'];
        }
        $result['data'] = $data;
        return new JsonResponse($result);
    }

    /**
     * @Route("/authors/add", name="authors-add",methods={"GET", "POST"})
     */
    public function addAction(Request $request)
    {
        $form = $this->createForm( AuthorForm::class, new Author(), array('validation_groups' => array('creation')) );

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $object = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($object);
                $em->flush();
                return $this->redirectToRoute('authors');
            }
        }

        return $this->render('AppBundle:Author:add.html.twig',array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/authors/edit/{id}", name="authors-edit",methods={"GET", "POST"}, requirements={"id" = "\d+"})
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $object = $em->find('AppBundle:Author',$id);
        $form = $this->createForm( AuthorForm::class, $object, array('validation_groups' => array('edition')));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $object = $form->getData();
                $em->merge($object);
                $em->flush();
                return $this->redirectToRoute('authors');
            }
        }

        return $this->render('AppBundle:Author:edit.html.twig',array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/authors/delete/{id}", name="authors-delete",methods={"GET", "POST"}, requirements={"id" = "\d+"})
     */
    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $object = $em->find('AppBundle:Author',$id);
        $token = $this->get('security.csrf.token_manager');
        if ($request->isMethod('POST')) {
            if ($request->get('token_') === $token->getToken('delete')->getValue()) {
                $em->remove($object);
                $em->flush();
            }
            return $this->redirectToRoute('authors');
        }

        return $this->render('AppBundle:Author:delete.html.twig',array(
            'object' => $object,
            'token' => $token->refreshToken('delete'),
            'id' => $id
        ));
    }

    /**
     * @Route("/authors/ajax/getAuthorsList", name="ajax_get_authors_list", methods={"GET"}, condition="request.isXmlHttpRequest()")
     */
    public function ajaxGetAuthorsList(Request $request)
    {
        $result = array();
        $limit = filter_var($request->get('page_limit'),FILTER_SANITIZE_NUMBER_INT);
        $search = '%'.filter_var(mb_strtolower($request->get('q')),FILTER_SANITIZE_STRING).'%';

        $em = $this->getDoctrine()->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('a')
            ->from('AppBundle:Author','a')
            ->where('LOWER(a.name) LIKE :search')
                ->setParameter('search',$search);
        $qb->orderBy('a.name','ASC')
            ->setMaxResults($limit);

        $objects = $qb->getQuery()->getResult();
        /** @var Author $object */
        foreach ($objects as $object)
        {
            $result[] = array(
                'id' => $object->getId(),
                'text' => $object->getName()
            );
        }

        return new JsonResponse($result);
    }

    /**
     * @Route("/authors/info/{id}", name="authors-info",methods={"GET"}, requirements={"id" = "\d+"})
     */
    public function infoAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('a')
            ->from('AppBundle:Author','a')
            ->where('a.id = ?1')
            ->setParameter(1,$id);
        $object = $qb->getQuery()->getSingleResult();

        $query = $qb->getQuery()->getSQL();
        $query = str_replace(',', ",<br>",$query);
        $query = str_replace('FROM', "<br>FROM",$query);
        $query= str_replace('WHERE', "<br>WHERE",$query);
        $query= str_replace('LEFT', "<br>LEFT",$query);
        $query= str_replace('ORDER', "<br>ORDER",$query);

        return $this->render('AppBundle:Author:info.html.twig',array(
            'object' => $object,
            'query' => $query
        ));
    }

    /**
     * @Route("/authors/ajax/getBooksList", name="ajax_get_authors_book_list", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function ajaxGetAuthorsBooksList(Request $request)
    {

        $result = array();
        $result['draw'] = $request->get('draw');
        $offset = $request->get('start');

        $limit = $request->get('length');
        $id = filter_var($request->get('id'),FILTER_SANITIZE_NUMBER_INT);

        $em = $this->getDoctrine()->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('b')
            ->from('AppBundle:Book','b')
            ->innerJoin('b.authors', 'a')
            ->where('a.id = :id')
            ->setParameter('id',$id);
        $qb->orderBy('b.name','ASC')
            ->setMaxResults($limit)
            ->setFirstResult($offset);

        $objects = $qb->getQuery()->getResult();

        $query = $qb->getQuery()->getSQL();
        $query = str_replace(',', ",<br>",$query);
        $query = str_replace('FROM', "<br>FROM",$query);
        $query= str_replace('WHERE', "<br>WHERE",$query);
        $query= str_replace('ORDER', "<br>ORDER",$query);
        $query= str_replace('INNER', "<br>INNER",$query);
        $result['query'] = '<br><br>'. $query;

        $data = array();
        /** @var Book $object */
        foreach ($objects as $object)
        {
            $data[] = array(
                $object->getName(),
                $object->getDescription()
            );
        }
        $result['data'] = $data;

        $qb = null;
        $qb = $em->createQueryBuilder();
        $result['recordsTotal'] = $qb->select('count(b.id)')
            ->from('AppBundle:Book', 'b')
            ->innerJoin('b.authors', 'a')
            ->where('a.id = :id')->setParameter('id',$id)->getQuery()->getSingleScalarResult();
        $query = $qb->getQuery()->getSQL();
        $query = str_replace(',', ",<br>",$query);
        $query = str_replace('FROM', "<br>FROM",$query);
        $query= str_replace('WHERE', "<br>WHERE",$query);
        $query= str_replace('ORDER', "<br>ORDER",$query);
        $query= str_replace('INNER', "<br>INNER",$query);
        $result['query'] .= '<br><br>'. $query;
        $result['recordsFiltered'] = $result['recordsTotal'];
        return new JsonResponse($result);
    }

    /**
     * @Route("/authors/ajax/getAuthorStats", name="ajax_get_author_stats", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function ajaxGetAuthorStats(Request $request)
    {
        $result = array();
        $id = filter_var($request->get('id'),FILTER_SANITIZE_NUMBER_INT);

        $query = "SELECT 
to_char(date_trunc('month',bh0_.issue_date),'YYYY-MM') AS month,
count(*) AS count
FROM book_history AS bh0_
INNER JOIN book_author AS ba0_ ON (ba0_.book_id = bh0_.book_id)
INNER JOIN authors AS a0_ ON (a0_.id = ba0_.author_id)
WHERE a0_.id = :id
GROUP by month
LIMIT 6";
        $em = $this->getDoctrine()->getEntityManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($query);
        $stmt->bindValue('id',$id);
        $stmt->execute();
        $result['data'] = $stmt->fetchAll();
        $result['query'] = $query;

        return new JsonResponse($result);
    }

}