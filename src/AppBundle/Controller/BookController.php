<?php
/**
 * Created by PhpStorm.
 * User: fominsk
 * Date: 10.10.2016
 * Time: 15:21
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Author;
use AppBundle\Entity\Book;
use AppBundle\Form\BookForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Query\Expr;

class BookController extends Controller
{
    /**
     * @Route("/books", name="books", methods={"GET"})
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Book:index.html.twig');
    }

    /**
     * @Route("/books/add", name="books-add",methods={"GET", "POST"})
     */
    public function addAction(Request $request)
    {
        $form = $this->createForm( BookForm::class, new Book(), array('validation_groups' => array('creation'), 'locale' => $request->getLocale()) );

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $object = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($object);
                $em->flush();
                return $this->redirectToRoute('books');
            }
        }

        return $this->render('AppBundle:Book:add.html.twig',array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/books/edit/{id}", name="books-edit",methods={"GET", "POST"}, requirements={"id" = "\d+"})
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $object = $em->find('AppBundle:Book',$id);
        $form = $this->createForm( BookForm::class, $object, array('validation_groups' => array('edition'), 'locale' => $request->getLocale()));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $object = $form->getData();
                $em->merge($object);
                $em->flush();
                return $this->redirectToRoute('books');
            }
        }

        return $this->render('AppBundle:Book:edit.html.twig',array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/books/delete/{id}", name="books-delete",methods={"GET", "POST"}, requirements={"id" = "\d+"})
     */
    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $object = $em->find('AppBundle:Book',$id);
        $token = $this->get('security.csrf.token_manager');
        if ($request->isMethod('POST')) {
            if ($request->get('token_') === $token->getToken('delete')->getValue()) {
                $em->remove($object);
                $em->flush();
            }
            return $this->redirectToRoute('books');
        }

        return $this->render('AppBundle:Book:delete.html.twig',array(
            'object' => $object,
            'token' => $token->refreshToken('delete'),
            'id' => $id
        ));
    }


    /**
     * @Route("/books/ajax/getBooksTable", name="ajax_get_books_table", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function ajaxGetBooksTable(Request $request)
    {
        $result = array();
        $result['draw'] = $request->get('draw');
        $offset = $request->get('start');
        $limit = $request->get('length');
        $order = $request->get('order');
        $search_ = $request->get('search');
        $searchValue = mb_strtolower($search_['value']);
        $columns = $request->get('columns');

        $orderColumn = $columns[$order[0]['column']]['name'];
        $orderDirection = strtoupper($order[0]['dir']);

        $em = $this->getDoctrine()->getEntityManager();

        $con = $em->getConnection();
        $search = '%'.$searchValue.'%';
        $where = 'WHERE b0_.deleted_at IS NULL';
        if (!empty($searchValue)) {
            $where .= ' AND (LOWER(b0_.name) LIKE :search OR ba0_.author_id IN (SELECT a2_.id FROM authors AS a2_ WHERE LOWER(a2_.name) LIKE :search))';
        }

        $query = "SELECT DISTINCT b0_.*,
bh0_.issue_date AS book_availability,
(SELECT string_agg(DISTINCT a1_.name,',') FROM book_author AS ba1_ INNER JOIN authors AS a1_ ON (a1_.id = ba1_.author_id) WHERE ba1_.book_id = b0_.id) as authors,
(SELECT string_agg(DISTINCT g1_.name,',') FROM book_genre AS bg1_ INNER JOIN genres AS g1_ ON (g1_.id = bg1_.genre_id) WHERE bg1_.book_id = b0_.id) as genres
FROM books AS b0_
INNER JOIN book_author AS ba0_ ON (ba0_.book_id = b0_.id)
INNER JOIN book_genre AS bg0_ ON (bg0_.book_id = b0_.id)
LEFT JOIN book_history AS bh0_ ON (bh0_.book_id = b0_.id AND bh0_.return_date IS NULL)
".$where."
GROUP BY b0_.id, ba0_.author_id, bh0_.id
ORDER BY ".$orderColumn.' '.$orderDirection . ' LIMIT ' . $limit . ' OFFSET '.$offset;

        $stmt = $con->prepare($query);

        if (!empty($searchValue)) {
        $stmt->bindValue('search',$search);
        }
        $stmt->execute();
        $objects = $stmt->fetchAll();

        $result['query'] = $query;


        $data = array();
        $translator = $this->container->get('translator');
        /**
         * @var Book $object
         */
        foreach ($objects as $object) {
            $options = '<div class="btn-group btn-group-xs" role="group" aria-label="...">';
            $options .= '<a href="'.$this->generateUrl('books-info',array( "id" => $object['id'] )).'" class="btn btn-default"><i class="fa fa-info-circle"></i></a>';
            $options .= '<a href="'.$this->generateUrl('books-edit',array( "id" => $object['id'] )).'" class="btn btn-default"><i class="fa fa-edit"></i></a>';
            $options .= '<a href="'.$this->generateUrl('books-delete',array( "id" => $object['id'] )).'" class="btn btn-default cancel"><i class="fa fa-trash-o"></i></a>';
            $options .= '</div>';

            if ($object['book_availability'] === null) {
                $status = '<span class="label label-success">'.$translator->trans('label.available').'</span>';
            } else {
                $status = '<span class="label label-danger">'.$translator->trans('label.issued').'</span>';
            }

            $data[] = array(
                $object['id'],
                $object['name'],
                $object['authors'],
                $object['genres'],
                $status,
                $options
            );
        }
        $qb = null;
        $qb = $em->createQueryBuilder();
        $result['recordsTotal'] = $qb->select('count(b.id)')->from('AppBundle:Book', 'b')->getQuery()->getSingleScalarResult();
        $query = $qb->getQuery()->getSQL();
        $query = str_replace(',', ",<br>",$query);
        $query = str_replace('FROM', "<br>FROM",$query);
        $query= str_replace('WHERE', "<br>WHERE",$query);
        $query= str_replace('ORDER', "<br>ORDER",$query);
        $query= str_replace('INNER', "<br>INNER",$query);
        $result['query'] .= '<br><br>'.$query;
        if ($searchValue !== '') {

            $query = "SELECT count(*) AS filtered_row_count 
FROM (SELECT count(DISTINCT b0_.id)
FROM books AS b0_
INNER JOIN book_author AS ba0_ ON (ba0_.book_id = b0_.id)
INNER JOIN authors AS a0_ ON (a0_.id = ba0_.author_id)
".$where."
GROUP BY b0_.id
) as num";


            $stmt = $con->prepare($query);
            if (!empty($searchValue)) {
                $stmt->bindValue('search',$search);
            }
            $stmt->execute();
            $count = $stmt->fetchColumn(0);
            $result['recordsFiltered'] = $count;


            $result['query'] .= '<br><br>'.$query;
        } else {
            $result['recordsFiltered'] = $result['recordsTotal'];
        }
        $result['data'] = $data;

        return new JsonResponse($result);
    }

    /**
     * @Route("/books/ajax/getBooksList", name="ajax_get_books_list", methods={"GET"}, condition="request.isXmlHttpRequest()")
     */
    public function ajaxGetBookList(Request $request)
    {
        $result = array();
        $limit = filter_var($request->get('page_limit'),FILTER_SANITIZE_NUMBER_INT);

        $em = $this->getDoctrine()->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('b')
            ->from('AppBundle:Book','b')->leftJoin('b.bookHistory', 'bh');
        if (ctype_digit($request->get('q'))) {
            $search = (int)filter_var(mb_strtolower($request->get('q')),FILTER_SANITIZE_NUMBER_INT);
            $qb->where('b.id = :search');
        } else {
            $search = '%'.filter_var(mb_strtolower($request->get('q')),FILTER_SANITIZE_STRING).'%';
            $qb->where('LOWER(b.name) LIKE :search');
        }
        $qb->setParameter('search',$search)->andWhere('(bh.return_date IS NOT NULL AND bh.issue_date IS NOT NULL) OR (bh.return_date IS NULL AND bh.issue_date IS NULL)')
            ->orderBy('b.name','ASC')
            ->setMaxResults($limit);

        $objects = $qb->getQuery()->getResult();
        /** @var Book $object */
        foreach ($objects as $object)
        {
            $result[] = array(
                'id' => $object->getId(),
                'text' => '[' . $object->getId() . '] ' . $object->getName() . ' ('. $object->getBookAuthors() . ')'
            );
        }

        return new JsonResponse($result);
    }

    /**
     * @Route("/books/info/{id}", name="books-info",methods={"GET", "POST"}, requirements={"id" = "\d+"})
     */
    public function infoAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('b','bh','r')
            ->from('AppBundle:Book','b')
            ->leftJoin('b.bookHistory','bh', Expr\Join::WITH, 'bh.return_date IS NULL')
            ->leftJoin('bh.reader','r')
            ->where('b.id = ?1')
            ->setParameter(1,$id);
        $object = $qb->getQuery()->getSingleResult();

        $query = $qb->getQuery()->getSQL();
        $query = str_replace(',', ",<br>",$query);
        $query = str_replace('FROM', "<br>FROM",$query);
        $query= str_replace('WHERE', "<br>WHERE",$query);
        $query= str_replace('LEFT', "<br>LEFT",$query);
        $query= str_replace('ORDER', "<br>ORDER",$query);

        return $this->render('AppBundle:Book:info.html.twig',array(
            'object' => $object,
            'history' => $object->getBookHistory()->first(),
            'query' => $query
        ));
    }

    /**
     * @Route("/readers/ajax/getBookStats", name="ajax_get_book_stats", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function ajaxGetReaderStats(Request $request)
    {
        $result = array();
        $id = filter_var($request->get('id'),FILTER_SANITIZE_NUMBER_INT);

        $query = "SELECT 
to_char(date_trunc('month',bh0_.issue_date),'YYYY-MM') AS month,
count(*) AS count
FROM book_history AS bh0_
WHERE bh0_.book_id = :id
GROUP by month
LIMIT 6";
        $em = $this->getDoctrine()->getEntityManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($query);
        $stmt->bindValue('id',$id);
        $stmt->execute();
        $result['data'] = $stmt->fetchAll();
        $result['query'] = $query;

        return new JsonResponse($result);
    }
}