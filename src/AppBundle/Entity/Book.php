<?php
/**
 * Created by PhpStorm.
 * User: fominsk
 * Date: 10.10.2016
 * Time: 15:30
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

/**
 * Class Book
 * @ORM\Table(name="books")
 * @ORM\Entity()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class Book
{
    use SoftDeleteableEntity;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=254)
     * @Assert\NotBlank(groups={"creation", "edition"})
     * @Assert\Length(min=1, max=150, groups={"creation", "edition"})
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Author", inversedBy="books")
     */
    private $authors;
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Genre", inversedBy="books")
     */
    private $genres;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(groups={"creation", "edition"})
     * @Assert\Length(min=1, max=1024, groups={"creation", "edition"})
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\BookHistory", mappedBy="book")
     */
    private $bookHistory;

    public function __construct()
    {
        $this->authors = new ArrayCollection();
        $this->genres = new ArrayCollection();
        $this->bookHistory = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * @param mixed $authors
     */
    public function setAuthors($authors)
    {
        $this->authors = $authors;
    }

    /**
     * @return mixed
     */
    public function getGenres()
    {
        return $this->genres;
    }

    /**
     * @param mixed $genres
     */
    public function setGenres($genres)
    {
        $this->genres = $genres;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getBookAuthors()
    {
        $authors = '';
        /** @var Author $author */
        foreach ($this->authors as $author)
        {
            $authors .= $author->getName().', ';
        }

        return rtrim($authors,', ');
    }

    public function getBookGenres()
    {
        $genres = '';
        /** @var Genre $genre */
        foreach ($this->genres as $genre)
        {
            $genres .= $genre->getName().', ';
        }

        return rtrim($genres,', ');
    }

    /**
     * @return mixed
     */
    public function getBookHistory()
    {
        return $this->bookHistory;
    }

    /**
     * @param mixed $bookHistory
     */
    public function setBookHistory($bookHistory)
    {
        $this->bookHistory = $bookHistory;
    }


}