<?php
/**
 * Created by PhpStorm.
 * User: fominsk
 * Date: 10.10.2016
 * Time: 15:30
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;
use JMS\Serializer\Annotation\Type;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

/**
 * Class Reader
 * @ORM\Table(name="readers")
 * @ORM\Entity()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class Reader
{
    use SoftDeleteableEntity;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     * @Assert\NotBlank(groups={"creation", "edition"})
     * @Assert\Length(min=3, max=150, groups={"creation", "edition"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=150)
     * @Assert\NotBlank(groups={"creation", "edition"})
     * @Assert\Length(min=3, max=150, groups={"creation", "edition"})
     */
    private $surname;

    /**
     * @Type("libphonenumber\PhoneNumber")
     * @ORM\Column(type="phone_number", nullable=true)
     * @AssertPhoneNumber(defaultRegion="LV", groups={"creation", "edition"})
     */
    private $phone;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\BookHistory", mappedBy="reader")
     */
    private $bookHistory;

    public function __construct()
    {
        $this->bookHistory = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getBookHistory()
    {
        return $this->bookHistory;
    }

    /**
     * @param mixed $bookHistory
     */
    public function setBookHistory($bookHistory)
    {
        $this->bookHistory = $bookHistory;
    }



}