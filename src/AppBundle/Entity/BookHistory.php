<?php
/**
 * Created by PhpStorm.
 * User: fominsk
 * Date: 07.12.2016
 * Time: 10:30
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class Book
 * @ORM\Table(name="book_history")
 * @ORM\Entity()
 */
class BookHistory
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Book", inversedBy="bookHistory")
     * @ORM\JoinColumn(name="book_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank(groups={"creation", "edition"})
     */
    private $book;

    /**
     * @ORM\Column(type="datetime")
     */
    private $issue_date;

    /**
     * @ORM\Column(type="date")
     */
    private $due_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $return_date;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Reader", inversedBy="bookHistory")
     * @ORM\JoinColumn(name="reader_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank(groups={"creation", "edition"})
     */
    private $reader;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="bookHistory")
     */
    private $librarian;

    public function __construct()
    {
        $this->issue_date = new \DateTime('now');
        $this->due_date = new \DateTime('now');
        $this->due_date->modify('+2 week');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * @param mixed $book
     */
    public function setBook($book)
    {
        $this->book = $book;
    }

    /**
     * @return mixed
     */
    public function getIssueDate()
    {
        return $this->issue_date;
    }

    /**
     * @param mixed $issue_date
     */
    public function setIssueDate($issue_date)
    {
        $this->issue_date = $issue_date;
    }

    /**
     * @return mixed
     */
    public function getDueDate()
    {
        return $this->due_date;
    }

    /**
     * @param mixed $due_date
     */
    public function setDueDate($due_date)
    {
        $this->due_date = $due_date;
    }

    /**
     * @return mixed
     */
    public function getReturnDate()
    {
        return $this->return_date;
    }

    /**
     * @param mixed $return_date
     */
    public function setReturnDate($return_date)
    {
        $this->return_date = $return_date;
    }

    /**
     * @return mixed
     */
    public function getReader()
    {
        return $this->reader;
    }

    /**
     * @param mixed $reader
     */
    public function setReader($reader)
    {
        $this->reader = $reader;
    }

    /**
     * @return mixed
     */
    public function getLibrarian()
    {
        return $this->librarian;
    }

    /**
     * @param mixed $librarian
     */
    public function setLibrarian($librarian)
    {
        $this->librarian = $librarian;
    }



}