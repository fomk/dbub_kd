<?php
/**
 * Created by PhpStorm.
 * User: fominsk
 * Date: 10.10.2016
 * Time: 10:04
 */

namespace AppBundle\Entity;


use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints as Assert;
use Rollerworks\Bundle\PasswordStrengthBundle\Validator\Constraints as RollerworksPassword;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use JMS\Serializer\Annotation\Type;

/**
 * Class User
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @UniqueEntity(fields={"username"}, groups={"creation", "edition"})
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class User
    implements AdvancedUserInterface, \Serializable
{
    use SoftDeleteableEntity;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     * @Assert\NotBlank(groups={"creation", "edition", "profile"})
     * @Assert\Length(min=2, max=150, groups={"creation", "edition", "profile"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=150)
     * @Assert\NotBlank(groups={"creation", "edition", "profile"})
     * @Assert\Length(min=2, max=150, groups={"creation", "edition", "profile"})
     */
    private $surname;

    /**
     * @var string $username
     *
     * @ORM\Column(name="username", type="string", unique=true, length=25)
     * @Assert\NotBlank(groups={"creation", "edition"})
     * @Assert\Length(min=3, max=25, groups={"creation", "edition"})
     */
    protected $username;

    /**
     * @ORM\Column(type="text")
     */
    private $password;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Assert\NotBlank(groups={"creation", "edition"})
     */
    private $roles;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     * @Assert\Type(type="bool", groups={"creation", "edition"})
     */
    private $isActive;

    /**
     * @Assert\NotBlank(groups={"creation", "password_change"})
     * @RollerworksPassword\PasswordRequirements(minLength=8, requireLetters=true, requireNumbers=true, requireCaseDiff=true, groups={"creation", "edition", "password_change"})
     */
    private $clearTextPassword;

    /**
     * @UserPassword(groups={"password_change"})
     * @Assert\NotBlank(groups={"password_change"})
     */
    private $oldPassword;
    /**
     * @ORM\Column(name="language", type="string", length=10)
     * @Assert\Language()
     */
    private $language;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\BookHistory", mappedBy="librarian")
     */
    private $bookHistory;

    public function __construct()
    {
        $this->isActive = true;
        $this->roles = array('ROLE_USER');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param mixed $roles
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return mixed
     */
    public function getClearTextPassword()
    {
        return $this->clearTextPassword;
    }

    /**
     * @param mixed $clearTextPassword
     */
    public function setClearTextPassword($clearTextPassword)
    {
        $this->clearTextPassword = $clearTextPassword;
    }

    /**
     * @return mixed
     */
    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    /**
     * @param mixed $oldPassword
     */
    public function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->isActive;
    }

    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->isActive,
            $this->language,
        ));
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->isActive,
            $this->language,
            ) = unserialize($serialized);
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return mixed
     */
    public function getBookHistory()
    {
        return $this->bookHistory;
    }

    /**
     * @param mixed $bookHistory
     */
    public function setBookHistory($bookHistory)
    {
        $this->bookHistory = $bookHistory;
    }


}